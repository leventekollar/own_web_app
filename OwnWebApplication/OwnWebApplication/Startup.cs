﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OwnWebApplication.Startup))]
namespace OwnWebApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
